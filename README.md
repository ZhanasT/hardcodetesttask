
# Тестовое задание




## Перед запуском

- Записать СonnectionString в appSetting.json или в user-secrets
- Выполнить update-database в Package Manager Console

```bash
  update-database
```
    
## API Reference


### Создать категорию
Для добавления дополнительных полей требуется перечислять названия полей через запятую

```http
  POST /api/Category/CreateCategory
```
```json
{
  "name": "string",
  "additionalFields": "additionalField1, additionalField2, additionalField3"
}
```
### Создать продукт

```http
  POST /api/Product/CreateProduct
```
```json
{
  "name": "string",
  "description": "string",
  "price": 0,
  "categoryName": "string",
  "additionalFields": {
    "additionalProp1": "string",
    "additionalProp2": "string",
    "additionalProp3": "string"
  }
}
```
### Получить список товаров по фильтру

```http
  POST /api/Product/GetFilteredProducts
```
```json
{
  "categoryName": "string",
  "additionalFields": {
    "additionalProp1": "string",
    "additionalProp2": "string",
    "additionalProp3": "string"
  }
}
```


### Получить детальную информацию о товаре

```http
  GET /api/Product/GetProductById/{id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required** |



