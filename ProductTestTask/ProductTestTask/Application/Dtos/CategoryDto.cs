﻿using System.ComponentModel.DataAnnotations;

namespace ProductTestTask.Application.Dtos
{
    public class CategoryDto
    {
        [Required]
        public string Name { get; set; } = String.Empty;
        public string? AdditionalFields { get; set; } = String.Empty;
    }
}
