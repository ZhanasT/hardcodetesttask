﻿using System.ComponentModel.DataAnnotations;

namespace ProductTestTask.Application.Dtos
{
    public class ProductDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public IDictionary<string, string>? AdditionalFields { get; set; }
    }
}
