﻿using ProductTestTask.Domain;

namespace ProductTestTask.Application.Dtos
{
    public class ProductWithAdditionalFields
    {
        public Product Product { get; set; }
        public List<AdditionalFieldRelation> AdditionalFields { get; set; }

    }
}
