﻿namespace ProductTestTask.Application.Dtos
{
    public class ProductFilterDto
    {
        public string CategoryName { get; set; } = String.Empty;
        public IDictionary<string, string>? AdditionalFields { get; set; }
    }
}
