﻿using ProductTestTask.Domain;

namespace ProductTestTask.Application.Persistance
{
    public interface ICategoryRepository
    {
        Task<Category> CreateAsync(Category category);
        Task<Category> GetByNameAsync(string name);
    }
}
