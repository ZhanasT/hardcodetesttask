﻿using ProductTestTask.Domain;

namespace ProductTestTask.Application.Persistance
{
    public interface IAdditionalFieldRelationRepository
    {
        Task AddInRangeAsync(List<AdditionalFieldRelation> additionalFieldRelations);
        Task<List<AdditionalFieldRelation>> GetByProductIdsAsync(List<int> productIds);
    }
}
