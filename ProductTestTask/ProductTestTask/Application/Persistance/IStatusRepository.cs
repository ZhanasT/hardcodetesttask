﻿using ProductTestTask.Domain;

namespace ProductTestTask.Application.Persistance
{
    public interface IStatusRepository
    {
        Task<Status> CreateAsync(Status status);
        Task AddRangeAsync(List<Status> status);
        Task<bool> AnyAsync();
        Task<Status> GetByNameAsync(string name);
    }
}
