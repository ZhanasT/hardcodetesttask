﻿namespace ProductTestTask.Application.Persistance
{
    public interface IUnitOfWork : IDisposable
    {
         IStatusRepository Statuses { get; }
         IProductRepository Products { get; }
         ICategoryRepository Categories { get; }
         IAdditionalFieldRelationRepository AdditionalFieldRelations { get; }
        public Task SaveAsync();
    }
}
