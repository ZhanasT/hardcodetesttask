﻿using ProductTestTask.Application.Dtos;
using ProductTestTask.Domain;

namespace ProductTestTask.Application.Persistance
{
    public interface IProductRepository
    {
        Task<Product> CreateAsync(Product product);
        Task<List<Product>> GetByFilterAsync(ProductFilterDto productFilterDto);
        Task<Product> GetByIdDetailedAsync(int id);
    }
}
