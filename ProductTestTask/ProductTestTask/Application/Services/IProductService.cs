﻿using ProductTestTask.Application.Dtos;
using ProductTestTask.Domain;

namespace ProductTestTask.Application.Services
{
    public interface IProductService
    {
        Task<Product> CreateAsync(ProductDto productDto);
        Task<List<ProductWithAdditionalFields>> GetFilteredProductsAsync(ProductFilterDto productFilterDto);
        Task<Product> GetByIdDetailedAsync(int id);
    }
}
