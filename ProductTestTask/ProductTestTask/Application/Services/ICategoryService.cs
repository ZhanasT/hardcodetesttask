﻿using ProductTestTask.Application.Dtos;
using ProductTestTask.Domain;

namespace ProductTestTask.Application.Services
{
    public interface ICategoryService
    {
        Task<Category> CreateAsync(CategoryDto categoryDto);
    }
}
