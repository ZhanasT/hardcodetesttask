﻿using ProductTestTask.Application.Persistance;
using ProductTestTask.Application.Services;
using ProductTestTask.Infrastructure.Repositories;
using ProductTestTask.Infrastructure.Services;

namespace ProductTestTask.Application.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddCustomServices(this IServiceCollection services)
        {
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IStatusService, StatusService>();
            services.AddTransient<IAdditionalFieldRelationService, AdditionalFieldRelationService>();
        }
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IStatusRepository, StatusRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IAdditionalFieldRelationRepository, AdditionalFieldRelationRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
