﻿using Microsoft.AspNetCore.Mvc;
using ProductTestTask.Application.Dtos;
using ProductTestTask.Application.Services;
using System.Reflection;

namespace ProductTestTask.Application.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;
        public ProductController(ILogger<ProductController> logger, IProductService productService)
        {
            _logger = logger;
            _productService = productService;
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductDto productDto)
        {
            var currentMethodName = MethodBase
                    .GetCurrentMethod()
                    .DeclaringType
                    .Name
                    .Substring(1)
                    .Split('>')[0];
            try
            {
                await _productService.CreateAsync(productDto);
                _logger.LogInformation($"Method executed: {currentMethodName}");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{currentMethodName}: {ex}");
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> GetFilteredProducts([FromBody] ProductFilterDto productFilterDto)
        {
            var currentMethodName = MethodBase
                    .GetCurrentMethod()
                    .DeclaringType
                    .Name
                    .Substring(1)
                    .Split('>')[0];
            try
            {
                var result = await _productService.GetFilteredProductsAsync(productFilterDto);
                _logger.LogInformation($"Method executed: {currentMethodName}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetProductById(int id)
        {
            var currentMethodName = MethodBase
                    .GetCurrentMethod()
                    .DeclaringType
                    .Name
                    .Substring(1)
                    .Split('>')[0];
            try
            {
                var result = await _productService.GetByIdDetailedAsync(id);
                _logger.LogInformation($"Method executed: {currentMethodName}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
