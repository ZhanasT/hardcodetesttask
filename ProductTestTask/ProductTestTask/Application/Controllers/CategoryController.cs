﻿using Microsoft.AspNetCore.Mvc;
using ProductTestTask.Application.Dtos;
using ProductTestTask.Application.Services;
using ProductTestTask.Domain;
using System.Reflection;

namespace ProductTestTask.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryService _categoryService;
        public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService)
        {
            _logger = logger;
            _categoryService = categoryService;
        }
        [HttpPost("[action]")]
        public async Task<IActionResult> CreateCategory(CategoryDto categoryDto)
        {
            var currentMethodName = MethodBase
                    .GetCurrentMethod()
                    .DeclaringType
                    .Name
                    .Substring(1)
                    .Split('>')[0];
            try
            {
                await _categoryService.CreateAsync(categoryDto);
                _logger.LogInformation($"Method executed: {currentMethodName}");
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{currentMethodName}: {ex}");
                return BadRequest(ex.Message);
            }
        }

    }
}
