
using Microsoft.EntityFrameworkCore;
using ProductTestTask.Application.Extensions;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;
using ProductTestTask.Infrastructure.Repositories;

namespace ProductTestTask
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            //��� ������������� timestamp without time zone
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn moure about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddDbContext<ApplicationDbContext>(
                options => options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));
            builder.Services.AddRepositories();
            builder.Services.AddCustomServices();



            var app = builder.Build();

            var scope = app.Services.CreateScope();
            scope.DataInitialize();
            

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}
