﻿namespace ProductTestTask.Domain
{
#nullable disable
    public class Status : BaseEntity
    {
        public string Name { get; set; } = String.Empty;
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }

    }
}
