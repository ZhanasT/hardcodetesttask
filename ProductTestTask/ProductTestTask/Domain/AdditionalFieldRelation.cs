﻿namespace ProductTestTask.Domain
{
    public class AdditionalFieldRelation : BaseEntity
    {
        public string FieldName { get; set; } = String.Empty;
        public string FieldValue { get; set;} = String.Empty;
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
