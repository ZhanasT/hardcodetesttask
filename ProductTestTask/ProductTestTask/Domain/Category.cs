﻿namespace ProductTestTask.Domain
{
    public class Category : BaseEntity
    {
        public string Name { get; set; } = String.Empty;
        public string? AdditionalFields { get; set; } = String.Empty;
        public List<Product> Products { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
    }
}
