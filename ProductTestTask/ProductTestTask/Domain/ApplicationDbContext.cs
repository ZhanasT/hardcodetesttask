﻿using Microsoft.EntityFrameworkCore;

namespace ProductTestTask.Domain
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<AdditionalFieldRelation> AdditionalFieldRelations { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Product>()
                .HasKey(p => p.Id);
            modelBuilder.Entity<Category>()
                .HasKey(c => c.Id);
            modelBuilder.Entity<Status>()
                .HasKey(s => s.Id);
            modelBuilder.Entity<AdditionalFieldRelation>()
                .HasKey(adr => adr.Id);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Category)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.CategoryId);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.Status)
                .WithMany(c => c.Products)
                .HasForeignKey(p => p.StatusId);

            modelBuilder.Entity<Category>()
                .HasOne(c => c.Status)
                .WithMany(s => s.Categories)
                .HasForeignKey(c => c.StatusId);

            modelBuilder.Entity<AdditionalFieldRelation>()
                .HasOne(afr => afr.Product)
                .WithMany(p => p.AdditionalFieldRelations)
                .HasForeignKey(afr => afr.ProductId);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}
