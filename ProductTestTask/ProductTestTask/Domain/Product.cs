﻿namespace ProductTestTask.Domain
{
#nullable disable
    public class Product : BaseEntity
    {
        public string Name { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public int StatusId { get; set; }
        public Status Status { get; set; }
        public List<AdditionalFieldRelation> AdditionalFieldRelations { get; set; }

    }
}
