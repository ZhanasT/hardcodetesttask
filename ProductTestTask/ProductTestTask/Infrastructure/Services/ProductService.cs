﻿using ProductTestTask.Application.Dtos;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Application.Services;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Services
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Product> CreateAsync(ProductDto productDto)
        {
            var activeStatus = await _unitOfWork.Statuses.GetByNameAsync("Active");
            int statusId = activeStatus.Id;
            var category = await _unitOfWork.Categories.GetByNameAsync(productDto.CategoryName);

            var productToCreate = new Product
            {
                Name = productDto.Name,
                Description = productDto.Description,
                Price = productDto.Price,
                CategoryId = category.Id,
                StatusId = statusId,
                CreatedAt = DateTime.Now,
            };

            Product createdProduct = null;

            if (category.AdditionalFields is not null && productDto.AdditionalFields is not null)
            {
                var additionalFieldKeys = category.AdditionalFields
                    .Split(',')
                    .Select(additionalFieldKey => additionalFieldKey
                    .Trim())
                    .ToList();

                if ((productDto.AdditionalFields!.Count != additionalFieldKeys.Count)
                    || !(productDto.AdditionalFields.Keys.OrderBy(x => x).SequenceEqual(additionalFieldKeys)))
                        throw new ArgumentException("Неверно указаны дополнительные поля выбранной категории");

                var additionalFieldRelationList = new List<AdditionalFieldRelation>(productDto.AdditionalFields.Count);

                foreach(var key in productDto.AdditionalFields.Keys)
                {
                    additionalFieldRelationList.Add(new AdditionalFieldRelation
                    {
                        FieldName = key,
                        FieldValue = productDto.AdditionalFields[key],
                        CreatedAt = DateTime.Now
                    });
                }
                productToCreate.AdditionalFieldRelations = additionalFieldRelationList;
                createdProduct = await _unitOfWork.Products.CreateAsync(productToCreate);
                await _unitOfWork.SaveAsync();
            }
            
            return createdProduct;
        }
        public async Task<List<ProductWithAdditionalFields>> GetFilteredProductsAsync(ProductFilterDto productFilterDto)
        {
            var productsByFilter = await _unitOfWork.Products.GetByFilterAsync(productFilterDto);

            var additionalFieldRelationFilter = new List<int>();

            productsByFilter.ForEach(p => additionalFieldRelationFilter.Add(p.Id));

            var additionalFieldRelationsByFilter = await _unitOfWork.AdditionalFieldRelations.GetByProductIdsAsync(additionalFieldRelationFilter);
            var result = new List<ProductWithAdditionalFields>(productsByFilter.Count);
            foreach (var product in productsByFilter)
            {
                var productToAdd = new ProductWithAdditionalFields
                {
                    Product = product,
                    AdditionalFields = additionalFieldRelationsByFilter.Where(afr => afr.ProductId == product.Id).ToList(),
                };
                result.Add(productToAdd);
            }
            return result;
        }
        public async Task<Product> GetByIdDetailedAsync(int id)
        {
            return await _unitOfWork.Products.GetByIdDetailedAsync(id);
        }

    }
}
