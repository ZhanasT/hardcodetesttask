﻿using ProductTestTask.Application.Dtos;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Application.Services;
using ProductTestTask.Domain;
using ProductTestTask.Infrastructure.Repositories;

namespace ProductTestTask.Infrastructure.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<Category> CreateAsync(CategoryDto categoryDto)
        {
            var activeStatus = await _unitOfWork.Statuses.GetByNameAsync("Active");
            int statusId = activeStatus.Id;

            if (String.IsNullOrEmpty(categoryDto.AdditionalFields))
                categoryDto.AdditionalFields = null;

            var categoryToCreate = new Category
            {
                Name = categoryDto.Name,
                AdditionalFields = categoryDto.AdditionalFields,
                CreatedAt = DateTime.Now,
                StatusId = statusId
            };
            var createdCategory = await _unitOfWork.Categories.CreateAsync(categoryToCreate);
            await _unitOfWork.SaveAsync();
            return createdCategory;
        }
    }
}
