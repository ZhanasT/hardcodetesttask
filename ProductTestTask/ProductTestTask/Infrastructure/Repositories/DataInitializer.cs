﻿using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public static class DataInitializer
    {
        public static async void DataInitialize(this IServiceScope serviceScope)
        {
            var services = serviceScope.ServiceProvider;
            try
            {
                var unitOfWork = services.GetRequiredService<IUnitOfWork>();
                await InitializeAsync(unitOfWork);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        private static async Task InitializeAsync(IUnitOfWork unitOfWork)
        {
            if (await unitOfWork.Statuses.AnyAsync())
                return;

            var statuses = new List<Status>
            {
                new Status {
                    Name = "Active",
                    CreatedAt = DateTime.Now,
                },
                new Status {
                    Name = "Deleted",
                    CreatedAt = DateTime.Now,
                },
                new Status {
                    Name = "Archived",
                    CreatedAt = DateTime.Now,
                }
            };
            try
            {
                await unitOfWork.Statuses.AddRangeAsync(statuses);
                await unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
        }
    }
    
}
