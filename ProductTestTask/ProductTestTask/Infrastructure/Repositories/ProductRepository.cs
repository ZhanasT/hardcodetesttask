﻿using Microsoft.EntityFrameworkCore;
using ProductTestTask.Application.Dtos;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public ProductRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<Product> CreateAsync(Product product)
        {
            await ContainsProduct(product.Name);
            await _dbContext.Products.AddAsync(product);
            return product;
        }
        public async Task<List<Product>> GetByFilterAsync(ProductFilterDto productFilterDto)
        {
            var query = _dbContext.Products
                .Include(p => p.Category)
                .Include(p => p.Status)
                .Where(p => p.Category.Name == productFilterDto.CategoryName && p.Status.Name == "Active")
                .Select(p => new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price,
                    Category = new Category
                    {
                        Name = p.Category.Name,
                    },
                    Status = new Status
                    {
                        Name = p.Status.Name,
                    }
                });

            if (productFilterDto.AdditionalFields != null && productFilterDto.AdditionalFields.Any())
            {
                var additionalFieldRelations = FilterAdditionalFieldRelations(productFilterDto.AdditionalFields);
                var productIdsWithMatchingAdditionalFields = additionalFieldRelations.Select(afr => afr.ProductId).Distinct();

                query = query.Where(p => productIdsWithMatchingAdditionalFields.Contains(p.Id));
            }

            return await query.ToListAsync();
        }
        public async Task<Product> GetByIdDetailedAsync(int id)
        {
            var product = await _dbContext.Products
                .Include(p => p.Category)
                .Include(p => p.Status)
                .Include(p => p.AdditionalFieldRelations)
                .Where(p => p.Id == id)
                .Select(p => new Product
                {
                    Id = p.Id,
                    Name = p.Name,
                    Description = p.Description,
                    Price = p.Price,
                    CategoryId = p.CategoryId,
                    Category = new Category 
                    { 
                        Name = p.Category.Name,
                        CreatedAt = p.Category.CreatedAt,
                        StatusId = p.Category.StatusId,
                        Id = p.Category.Id,
                    },
                    StatusId = p.StatusId,
                    Status = new Status
                    {
                        Id = p.Status.Id,
                        Name = p.Status.Name,
                        CreatedAt = p.Status.CreatedAt,
                    },
                    AdditionalFieldRelations = new List<AdditionalFieldRelation>(),
                    CreatedAt = p.CreatedAt,
                })
                .FirstOrDefaultAsync();

            if (product is null)
                throw new ArgumentException("Данный продукт не существует");

            product.AdditionalFieldRelations = await _dbContext.AdditionalFieldRelations
                .Where(afr => afr.ProductId == id)
                .Select(afr => new AdditionalFieldRelation
                {
                    Id = afr.Id,
                    FieldName = afr.FieldName,
                    FieldValue = afr.FieldValue,
                    CreatedAt = afr.CreatedAt,
                })
                .ToListAsync();

            return product;

        }
        private async Task ContainsProduct(string name)
        {
            if (await _dbContext.Products.AnyAsync(c => c.Name == name))
                throw new ArgumentException("Данный продукт уже существует");
        }
        private IQueryable<AdditionalFieldRelation> FilterAdditionalFieldRelations(IDictionary<string, string> filterParams)
        {
            IQueryable<AdditionalFieldRelation> query = _dbContext.AdditionalFieldRelations;

            foreach (var kvp in filterParams)
            {
                string key = kvp.Key;
                string value = kvp.Value;

                query = query.Where(afr => afr.FieldName == key && afr.FieldValue == value);

            }
            return query;
        }
    }
}
