﻿using Microsoft.EntityFrameworkCore;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public class CategoryRepository : ICategoryRepository 
    {
        private readonly ApplicationDbContext _dbContext;
        public CategoryRepository(ApplicationDbContext dbContext) 
        { 
            _dbContext = dbContext;
        }
        public async Task<Category> CreateAsync(Category category)
        {
            if (await _dbContext.Categories.AnyAsync(c => c.Name == category.Name))
                throw new ArgumentException("Данная категория уже существует");

            await _dbContext.Categories.AddAsync(category);
            return category;
        }
        public async Task<Category> GetByNameAsync(string name)
        {
            if (!(await _dbContext.Categories.AnyAsync(c => c.Name == name)))
                throw new ArgumentException("Данной категории не существует");
            return await _dbContext.Categories.SingleOrDefaultAsync(c => c.Name == name);
        }
    }
}
