﻿using Microsoft.EntityFrameworkCore;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public class StatusRepository : IStatusRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public StatusRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Status> CreateAsync(Status status)
        {
            _ = await _dbContext.Statuses.AddAsync(status);
            return status;
        }
        public async Task AddRangeAsync(List<Status> statuses)
        {
            await _dbContext.Statuses.AddRangeAsync(statuses);
        }
        public async Task<bool> AnyAsync()
        {
            return await _dbContext.Statuses.AnyAsync();
        }
        public async Task<Status> GetByNameAsync(string name)
        {
            return await _dbContext.Statuses.FirstOrDefaultAsync(s => s.Name == name);
        }
    }
}
