﻿using Microsoft.EntityFrameworkCore;
using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public class AdditionalFieldRelationRepository : IAdditionalFieldRelationRepository
    {
        private readonly ApplicationDbContext _dbContext;
        public AdditionalFieldRelationRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task AddInRangeAsync(List<AdditionalFieldRelation> additionalFieldRelations)
        {
            await _dbContext.AdditionalFieldRelations.AddRangeAsync(additionalFieldRelations);
        }
        public async Task<List<AdditionalFieldRelation>> GetByProductIdsAsync(List<int> productIds)
        {
            return await _dbContext.AdditionalFieldRelations.Where(afr => productIds.Contains(afr.ProductId)).ToListAsync();
        }
    }
}
