﻿using ProductTestTask.Application.Persistance;
using ProductTestTask.Domain;

namespace ProductTestTask.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;

        public UnitOfWork(ApplicationDbContext dbContext,
                          IStatusRepository statusRepository,
                          ICategoryRepository categoryRepository,
                          IProductRepository productRepository,
                          IAdditionalFieldRelationRepository additionalFieldRelationRepository)
        {
            _dbContext = dbContext;
            Statuses = statusRepository;
            Categories = categoryRepository;
            Products = productRepository;
            AdditionalFieldRelations = additionalFieldRelationRepository;
        }

        public IStatusRepository Statuses { get; }
        public ICategoryRepository Categories { get; }
        public IProductRepository Products { get; }
        public IAdditionalFieldRelationRepository AdditionalFieldRelations { get; }

        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
